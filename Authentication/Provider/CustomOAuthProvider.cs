﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Authentication.Provider
{
    public class CustomOAuthProvider: OAuthAuthorizationServerProvider
    {
        string clientId = string.Empty;
        private async Task<List<Claim>> ReadDataAsync(string apiAccount,string ipAddress,string Username,string Password)
        {
            List<Claim> claims = new List<Claim>();
            if(Username=="admin" && Password=="admin")
            {
                claims.Add(new Claim(ClaimTypes.Role, "admin"));
                claims.Add(new Claim("apiaccount", apiAccount));
            }
            return claims;
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string tmpStr = context.Request.Query["apiAccount"];

            //Guid apiAccount;
            //if (!Guid.TryParse(tmpStr, out apiAccount))
            //{
            //    context.SetError("invalid_grant", "Invalid application account.");
            //    return;
            //}
            string ipAddress = context.Request.RemoteIpAddress;
            List<Claim> claimList = await ReadDataAsync(tmpStr, ipAddress, context.UserName, context.Password);
            
            if (claimList == null || (claimList != null && claimList.Count() == 0))
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }


            ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claimList);
            var ticket = new AuthenticationTicket(oAuthIdentity, null);

            context.Validated(ticket);
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientSecret = string.Empty;
            if(!context.TryGetBasicCredentials(out clientId,out clientSecret))
            {

            }
            context.Validated();
         //   return Task.FromResult<object>(null);

        }
    }
}